var painelLeitura = $('#painel-leitura');
var velocidade = 0;
var enabled = false;
var autoScroll;

autoScroll = setInterval(avancaPainel, 150 - (velocidade * 20));

function avancaPainel() {
	if (enabled) {
		painelLeitura.scrollTop(painelLeitura.scrollTop() + 1);
	}
	clearInterval(autoScroll);
	autoScroll = setInterval(avancaPainel, 150 - (velocidade * 10));
}

$('#botao-fullscreen').click(function() {
	var divLeitura = $('#div-leitura');
	var painelLeitura = $('#leitura-capitulo');
	
	if (!divLeitura.hasClass('fullscreen')) {
		divLeitura.addClass('fullscreen');
		painelLeitura.addClass('fullscreen');
	} else {
		divLeitura.removeClass('fullscreen');
		painelLeitura.removeClass('fullscreen');
	}
});

$('#botao-auto-scroll').click(function() {
	if (!enabled) {
		enabled = true;
	} else {
		enabled = false;
	}
});

$('#aumentar-velocidade').click(function() {
	velocidade++;
	enabled = true;
});

$('#diminuir-velocidade').click(function() {
	velocidade--;
	enabled = true;
});

function alteraTamanhoLetra(tamanhoLetra) {
	$('#painel-leitura').css('font-size', tamanhoLetra + 'px');
	$.get('/usuarios/setTamanhoFonte/' + tamanhoLetra);
}

$('#botao-diminuir').click(
		function() {
			var tamanhoLetra = parseInt($('#painel-leitura').css('font-size')
					.split('px')[0]);
			tamanhoLetra -= 2;
			alteraTamanhoLetra(tamanhoLetra);
		});
$('#botao-aumentar').click(
		function() {
			var tamanhoLetra = parseInt($('#painel-leitura').css('font-size')
					.split('px')[0]);
			tamanhoLetra += 2;
			alteraTamanhoLetra(tamanhoLetra);
		});

$.get('/usuarios/getTamanhoFonte', function(resultado) {
	alteraTamanhoLetra(resultado);
});