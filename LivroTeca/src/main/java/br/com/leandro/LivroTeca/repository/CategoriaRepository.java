package br.com.leandro.LivroTeca.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.leandro.LivroTeca.model.Categoria;

public interface CategoriaRepository extends CrudRepository<Categoria, Long> {

}
