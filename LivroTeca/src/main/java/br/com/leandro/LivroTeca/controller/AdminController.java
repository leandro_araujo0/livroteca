package br.com.leandro.LivroTeca.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.leandro.LivroTeca.model.Categoria;
import br.com.leandro.LivroTeca.repository.CategoriaRepository;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private CategoriaRepository categoriaRepository;

	@ModelAttribute(name = "categorias")
	public Iterable<Categoria> categorias() {
		return categoriaRepository.findAll();
	}
	
	@GetMapping("/painel")
	public String painel() {
		return "admin/painel";
	}
}
