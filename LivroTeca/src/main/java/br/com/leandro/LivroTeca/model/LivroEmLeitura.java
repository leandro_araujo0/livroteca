package br.com.leandro.LivroTeca.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Embeddable
public class LivroEmLeitura {

	@OneToOne
	private Livro livro;
	@Column(nullable = true)
	private long numeroCapitulo;
	@Enumerated(EnumType.STRING)
	private Status status;

	public LivroEmLeitura() {
	}

	public LivroEmLeitura(Livro livro, long numeroCapitulo, Status status) {
		this.livro = livro;
		this.numeroCapitulo = numeroCapitulo;
		this.status = status;
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public long getNumeroCapitulo() {
		return numeroCapitulo;
	}

	public void setNumeroCapitulo(long numeroCapitulo) {
		this.numeroCapitulo = numeroCapitulo;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((livro == null) ? 0 : livro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LivroEmLeitura other = (LivroEmLeitura) obj;
		if (livro == null) {
			if (other.livro != null)
				return false;
		} else if (!livro.equals(other.livro))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LivroEmLeitura [livro=" + livro + ", numeroCapitulo=" + numeroCapitulo + ", status=" + status + "]";
	}

}
