package br.com.leandro.LivroTeca.model;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;

@Entity
public class Usuario {

	@Id
	public String nomeDeUsuario;
	public String senha;
	public String nome;
	public String sobreNome;
	public String email;
	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> authorities;
	public boolean enabled;
	@ElementCollection(fetch = FetchType.EAGER)
	private Set<LivroEmLeitura> estante;
	private long tamanhoFonte;

	public Usuario() {
		this.authorities = new LinkedList<>();
		this.estante = new HashSet<>();
		tamanhoFonte = 16;
	}

	public String getNomeDeUsuario() {
		return nomeDeUsuario;
	}

	public void setNomeDeUsuario(String nomeDeUsuario) {
		this.nomeDeUsuario = nomeDeUsuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobreNome() {
		return sobreNome;
	}

	public void setSobreNome(String sobreNome) {
		this.sobreNome = sobreNome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void addAuthority(String authority) {
		this.authorities.add(authority);
	}

	public List<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "Usuario [nomeDeUsuario=" + nomeDeUsuario + ", senha=" + senha + ", nome=" + nome + ", sobreNome="
				+ sobreNome + ", email=" + email + ", authorities=" + authorities + ", enabled=" + enabled + "]";
	}

	public long estaLendo(Livro livro) {
		for (LivroEmLeitura livroEmLeitura : estante) {
			if (livroEmLeitura.getLivro().equals(livro)) {
				return livroEmLeitura.getNumeroCapitulo();
			}
		}
		return 0;
	}

	public Set<LivroEmLeitura> getEstante() {
		return estante;
	}

	public void setEstante(Set<LivroEmLeitura> estante) {
		this.estante = estante;
	}

	public void estaLendo(Livro livro, long numeroCapitulo) {
		for (LivroEmLeitura livroEmLeitura : estante) {
			if (livroEmLeitura.getLivro().equals(livro)) {
				livroEmLeitura.setNumeroCapitulo(numeroCapitulo);
				return;
			}
		}
		this.estante.add(new LivroEmLeitura(livro, numeroCapitulo, Status.LENDO));
	}

	public void finalizaLeitura(Livro livro) {
		LivroEmLeitura removeLivro = null;
		for (LivroEmLeitura livroEmLeitura : estante) {
			if (livroEmLeitura.getLivro().equals(livro)) {
				removeLivro = livroEmLeitura;
			}
		}
		if (removeLivro != null) {
			estante.remove(removeLivro);
		}
	}

	public void setTamanhoFonte(long tamanhoFonte) {
		this.tamanhoFonte = tamanhoFonte;
	}
	
	public long getTamanhoFonte() {
		return this.tamanhoFonte;
	}

}
