package br.com.leandro.LivroTeca.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.leandro.LivroTeca.model.Categoria;
import br.com.leandro.LivroTeca.repository.CategoriaRepository;

@Controller
@RequestMapping("/admin/categorias")
public class AdminCategoriaController {
	
	@Autowired
	private CategoriaRepository categoriaRepository;

	@GetMapping("/")
	public String categorias(Model model) {
		model.addAttribute("categorias", categoriaRepository.findAll());
		return "admin/categoria/lista";
	}
	
	@GetMapping("/form")
	public String form(Categoria categoria) {
		return "admin/categoria/form";
	}
	
	@GetMapping("/{id}/form")
	public String form(@PathVariable long id, Categoria categoria) {
		return "admin/categoria/form";
	}
	
	@PostMapping("/salva")
	public String salva(Categoria categoria) {
		categoriaRepository.save(categoria);
		return "redirect:/admin/categorias/";
	}
	
	@GetMapping("/{id}/deleta")
	public String deleta(@PathVariable long id) {
		categoriaRepository.delete(id);
		return "redirect:/admin/categorias/";
	}
}
