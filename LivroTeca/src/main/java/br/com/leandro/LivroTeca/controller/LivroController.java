package br.com.leandro.LivroTeca.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.leandro.LivroTeca.model.Capitulo;
import br.com.leandro.LivroTeca.model.Categoria;
import br.com.leandro.LivroTeca.model.Livro;
import br.com.leandro.LivroTeca.model.LivroPagingRepository;
import br.com.leandro.LivroTeca.model.Usuario;
import br.com.leandro.LivroTeca.repository.CapituloRepository;
import br.com.leandro.LivroTeca.repository.CategoriaRepository;
import br.com.leandro.LivroTeca.repository.LivroRepository;
import br.com.leandro.LivroTeca.repository.UsuarioRepository;

@Controller
@RequestMapping("/livros")
public class LivroController {

	@Autowired
	private LivroRepository livroRepository;
	@Autowired
	private CapituloRepository capituloRepository;
	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private LivroPagingRepository livroPagingRepository;
	@Autowired
	private CategoriaRepository categoriaRepository;

	private final int livrosPorPagina = 12;

	@ModelAttribute(name = "categorias")
	public Iterable<Categoria> categorias() {
		return categoriaRepository.findAll();
	}

	@GetMapping("/{numeroPagina}")
	public String livros(Model model, @PathVariable int numeroPagina) {
		PageRequest pageable = new PageRequest(numeroPagina - 1, livrosPorPagina);
		Page<Livro> page = livroPagingRepository.findAll(pageable);

		int totalPaginas = page.getTotalPages();

		model.addAttribute("livros", page.getContent());
		model.addAttribute("numeroPagina", numeroPagina);
		model.addAttribute("totalPaginas", totalPaginas);
		return "livros/lista";
	}

	@GetMapping("/{id}/{nome}/detalhes")
	public String detalhes(@PathVariable long id, Model model, Principal principal) {
		Livro livro = livroRepository.findOne(id);
		Usuario usuario = usuarioRepository.findByNomeDeUsuario(principal.getName());

		long numeroCapitulo = usuario.estaLendo(livro);
		Capitulo capitulo = capituloRepository.findTop1ByLivroOrderByNumeroDesc(livro);
		if (capitulo != null)
			model.addAttribute("numeroUltimoCapitulo", capitulo.getNumero());

		model.addAttribute("numeroCapitulo", numeroCapitulo);
		model.addAttribute("livro", livro);
		return "livros/detalhes";
	}

	@GetMapping("/{id}/{nome}/leitura/capitulo/{numeroCapitulo}")
	public String leitura(@PathVariable long id, @PathVariable long numeroCapitulo, Model model, Principal principal) {
		Livro livro = livroRepository.findOne(id);

		Usuario usuario = usuarioRepository.findByNomeDeUsuario(principal.getName());
		usuario.estaLendo(livro, numeroCapitulo);
		usuarioRepository.save(usuario);

		model.addAttribute("livro", livro);
		model.addAttribute("capitulo", capituloRepository.findByNumeroAndLivro(numeroCapitulo, livro));
		Capitulo capitulo = capituloRepository.findTop1ByLivroOrderByNumeroDesc(livro);
		if (capitulo != null) {
			model.addAttribute("numeroUltimoCapitulo", capitulo.getNumero());
			return "livros/leitura";
		} else {
			return "redirect:/error";
		}
	}

	@GetMapping("/procurar")
	public String procurar(String chave, String categoria, Model model) {
		if (categoria.equals("TODAS")) {
			categoria = "";
		}
		
		model.addAttribute("livros", livroRepository.findByChavePesquisaAndCategoria(chave, categoria));
		model.addAttribute("chave", chave);
		return "livros/resultadosPesquisa";
	}

	@GetMapping("/{id}/finalizarLeitura")
	public String finalizarLeitura(@PathVariable long id, Principal principal) {
		Livro livro = livroRepository.findOne(id);
		Usuario usuario = usuarioRepository.findByNomeDeUsuario(principal.getName());
		usuario.finalizaLeitura(livro);
		usuarioRepository.save(usuario);
		return "redirect:/";
	}
}
