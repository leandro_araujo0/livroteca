package br.com.leandro.LivroTeca.controller;

import java.security.Principal;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.leandro.LivroTeca.model.Usuario;

@Controller
public class HomeController implements ErrorController{
	
	private static final String errorPath = "/error";
	
	@RequestMapping("/")
	public String home() {
		return "redirect:/livros/1";
	}
	
	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
	@GetMapping("/cadastro")
	public String cadastro(Usuario usuario) {
		return "cadastro";
	}

	@Override
	public String getErrorPath() {
		return errorPath;
	}
	
	@GetMapping(errorPath)
	public String error(Principal principal) {
		return "erro";
	}

}
