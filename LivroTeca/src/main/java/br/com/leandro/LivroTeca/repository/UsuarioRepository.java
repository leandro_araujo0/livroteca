package br.com.leandro.LivroTeca.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.leandro.LivroTeca.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{

	public Usuario findByNomeDeUsuario(String name);

}
