package br.com.leandro.LivroTeca.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.leandro.LivroTeca.model.Usuario;
import br.com.leandro.LivroTeca.repository.UsuarioRepository;

@Controller
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@PostMapping("/salva")
	public String salva(Usuario usuario) {
		usuario.addAuthority("usuario");
		usuario.setEnabled(true);
		System.out.println(usuario);
		usuarioRepository.save(usuario);
		return "redirect:/login";
	}

	@ResponseBody
	@GetMapping(value = { "/setTamanhoFonte/{tamanhoFonte}", "/LivroTeca/setTamanhoFonte/{tamanhoFonte}" })
	public void setTamanhoFonte(@PathVariable long tamanhoFonte, Principal principal) {
		Usuario usuario = usuarioRepository.findByNomeDeUsuario(principal.getName());
		usuario.setTamanhoFonte(tamanhoFonte);
		usuarioRepository.save(usuario);
	}

	@ResponseBody
	@GetMapping(value = { "/getTamanhoFonte", "/LivroTeca/getTamanhoFonte" })
	public long getTamanhoFonte(Principal principal) {
		return usuarioRepository.findByNomeDeUsuario(principal.getName()).getTamanhoFonte();
	}
}
