package br.com.leandro.LivroTeca.model;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface LivroPagingRepository extends PagingAndSortingRepository<Livro, Long> {

}
