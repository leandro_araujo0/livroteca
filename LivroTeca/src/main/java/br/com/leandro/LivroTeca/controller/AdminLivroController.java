package br.com.leandro.LivroTeca.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.leandro.LivroTeca.model.Capitulo;
import br.com.leandro.LivroTeca.model.Livro;
import br.com.leandro.LivroTeca.repository.CapituloRepository;
import br.com.leandro.LivroTeca.repository.CategoriaRepository;
import br.com.leandro.LivroTeca.repository.LivroRepository;

@Controller
@RequestMapping("/admin/livros/")
public class AdminLivroController {

	@Autowired
	private LivroRepository livroRepository;
	@Autowired
	private CapituloRepository capituloRepository;
	@Autowired
	private CategoriaRepository categoriaRepository;

	@GetMapping("/")
	public String livros(Model model) {
		model.addAttribute("livros", livroRepository.findAll());
		return "/admin/livros/lista";
	}

	@GetMapping("/form")
	public String form(Livro livro, Model model) {
		model.addAttribute("categorias", categoriaRepository.findAll());
		return "/admin/livros/formLivro";
	}
	
	@GetMapping("/{id}/form")
	public String form(@PathVariable long id, Model model) {
		model.addAttribute("categorias", categoriaRepository.findAll());
		model.addAttribute("livro", livroRepository.findOne(id));
		return "/admin/livros/formLivro";
	}
	
	@PostMapping("/salva")
	public String salva(Livro livro) {
		livroRepository.save(livro);
		return "redirect:/admin/livros/";
	}
	
	@GetMapping("/{id}/{descricao}/detalhes")
	public String detalhesLivro(@PathVariable long id, Model model) {
		Livro livro = livroRepository.findOne(id);
		model.addAttribute("livro", livro);
		model.addAttribute("capitulos", capituloRepository.findByLivro(livro));
		return "admin/livros/detalhesLivro";
	}
	
	@GetMapping("/{idLivro}/{descricao}/capitulo/form")
	public String capituloForm(@PathVariable long idLivro, Capitulo capitulo, Model model) {
		Livro livro = livroRepository.findOne(idLivro);
		capitulo.setLivro(livro);
		model.addAttribute("capitulo", capitulo);
		model.addAttribute("livro", livro);
		return "/admin/livros/formCapitulo";
	}
	
	@GetMapping("/{idLivro}/{descricao}/capitulo/{idCapitulo}/detalhes")
	public String detalhesCapitulo(@PathVariable long idLivro, @PathVariable long idCapitulo, Model model) {
		model.addAttribute("livro", livroRepository.findOne(idLivro));
		model.addAttribute("capitulo", capituloRepository.findOne(idCapitulo));
		return "admin/livros/detalhesCapitulo";
	}
	
	@PostMapping("/capitulo/salva")
	public String salva(Capitulo capitulo) {
		capituloRepository.save(capitulo);
		return "redirect:/admin/livros/" + capitulo.getLivro().getId() + "/" + capitulo.getLivro().getTitulo() + "/detalhes";  
	}
}
