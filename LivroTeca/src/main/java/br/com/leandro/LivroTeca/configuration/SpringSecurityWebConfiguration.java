package br.com.leandro.LivroTeca.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SpringSecurityWebConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource dataSource;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/", "/livros/", "/livros/procurar", "/livros/*", "/css/**", "/img/**", "/js/**").permitAll()
		.antMatchers("/cadastro", "/usuarios/salva").permitAll()
		.antMatchers("/admin/**").hasAnyAuthority("admin")
		.anyRequest().authenticated()
		.and().formLogin().loginPage("/login").permitAll()
		.and().logout().permitAll().logoutSuccessUrl("/");
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource).usersByUsernameQuery("select nome_de_usuario, senha, enabled from usuario where nome_de_usuario = ?").authoritiesByUsernameQuery("select usuario_nome_de_usuario, authorities from usuario_authorities where usuario_nome_de_usuario = ?");
	}
	
}
