package br.com.leandro.LivroTeca;

import java.util.LinkedList;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import br.com.leandro.LivroTeca.model.Capitulo;
import br.com.leandro.LivroTeca.model.Categoria;
import br.com.leandro.LivroTeca.model.Livro;
import br.com.leandro.LivroTeca.model.Usuario;
import br.com.leandro.LivroTeca.repository.CapituloRepository;
import br.com.leandro.LivroTeca.repository.CategoriaRepository;
import br.com.leandro.LivroTeca.repository.LivroRepository;
import br.com.leandro.LivroTeca.repository.UsuarioRepository;

@SuppressWarnings("unused")
@SpringBootApplication
public class LivroTeca extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(LivroTeca.class, args);
	}

//	@Bean
//	public CommandLineRunner commandLineRunner(LivroRepository livroRepository, CapituloRepository capituloRepository, UsuarioRepository usuarioRepository, CategoriaRepository categoriaRepository) {
//		return args -> {
//			Livro livro = new Livro();
//			livro.setNome("TITULO");
//			livro.setDescricao("DESCRICAO");
//			livro.setSinopse("SINOPSE");
//			livro.setImagemCapa("Imagem do livro");
//			Categoria categoria = new Categoria();
//			categoria.setNome("INFANTO-JUVENIL");
//			categoriaRepository.save(categoria);
//			livro.setCategoria(categoria);
//			livroRepository.save(livro);
//
//			Capitulo capitulo = new Capitulo();
//			capitulo.setLivro(livro);
//			capitulo.setNumero(1);
//			capitulo.setTitulo("1");
//			capitulo.setTexto("CONTEUDO DO CAPITULO");
//			capituloRepository.save(capitulo);
//			
//			capitulo = new Capitulo();
//			capitulo.setLivro(livro);
//			capitulo.setNumero(2);
//			capitulo.setTitulo("O vidro que sumiu");
//			capitulo.setTexto("conteudo do segundo capitulo");
//			capituloRepository.save(capitulo);
//			
//			Usuario usuario = new Usuario();
//			usuario.setNomeDeUsuario("leandro");
//			usuario.setSenha("leandro");
//			usuario.setEnabled(true);
//			List<String> authorities = new LinkedList<>();
//			authorities.add("user");
//			usuario.setAuthorities(authorities);
//			usuarioRepository.save(usuario);
//		};
//	}
}
