package br.com.leandro.LivroTeca.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.leandro.LivroTeca.model.Capitulo;
import br.com.leandro.LivroTeca.model.Livro;

public interface CapituloRepository extends CrudRepository<Capitulo, Long> {

	public Iterable<Capitulo> findByLivro(Livro livro);

	public Capitulo findByNumeroAndLivro(long numeroCapitulo, Livro livro);

	public Capitulo findTop1ByLivroOrderByNumeroDesc(Livro livro);

}
