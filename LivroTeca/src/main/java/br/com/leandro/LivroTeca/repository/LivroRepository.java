package br.com.leandro.LivroTeca.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.leandro.LivroTeca.model.Livro;

public interface LivroRepository extends CrudRepository<Livro, Long> {

	@Query("select l from Livro l where (l.titulo like concat('%', :chave, '%') or l.descricao like concat('%', :chave, '%') or l.sinopse like concat('%', :chave, '%')) and (l.categoria like concat('%', :categoria, '%'))")
	public Iterable<Livro >findByChavePesquisaAndCategoria(@Param("chave") String chave, @Param("categoria") String categoria);

}
